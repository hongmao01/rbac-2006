package cn.jasonone.mapper;

import cn.jasonone.bean.Permission;
import cn.jasonone.bean.RolePermissionMapping;
import cn.jasonone.bean.RolePermissionMappingExample;
import java.util.List;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

@Mapper
public interface RolePermissionMappingMapper {
	long countByExample(RolePermissionMappingExample example);
	
	int deleteByExample(RolePermissionMappingExample example);
	
	int insertSelective(RolePermissionMapping record);
	
	List<RolePermissionMapping> selectByExample(RolePermissionMappingExample example);
	
	int updateByExampleSelective(@Param("record") RolePermissionMapping record, @Param("example") RolePermissionMappingExample example);
	
	int updateByExample(@Param("record") RolePermissionMapping record, @Param("example") RolePermissionMappingExample example);
	
	List<Permission> selectByRoleIdForPermissions(int roleId);
}