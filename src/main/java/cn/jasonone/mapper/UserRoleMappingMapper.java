package cn.jasonone.mapper;

import cn.jasonone.bean.Role;
import cn.jasonone.bean.UserRoleMapping;
import cn.jasonone.bean.UserRoleMappingExample;
import java.util.List;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

@Mapper
public interface UserRoleMappingMapper {
	long countByExample(UserRoleMappingExample example);
	
	int deleteByExample(UserRoleMappingExample example);
	
	int insertSelective(UserRoleMapping record);
	
	List<UserRoleMapping> selectByExample(UserRoleMappingExample example);
	
	int updateByExampleSelective(@Param("record") UserRoleMapping record, @Param("example") UserRoleMappingExample example);
	
	int updateByExample(@Param("record") UserRoleMapping record, @Param("example") UserRoleMappingExample example);
	
	List<Role> selectByUserIdForRoles(int userId);
}