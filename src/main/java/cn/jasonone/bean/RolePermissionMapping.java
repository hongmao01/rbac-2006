package cn.jasonone.bean;

import java.io.Serializable;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
    * 角色权限映射表
    */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class RolePermissionMapping implements Serializable {
    /**
    * 角色主键
    */
    private Integer roleId;

    /**
    * 权限主键
    */
    private Integer permissionId;

    private static final long serialVersionUID = 1L;
}