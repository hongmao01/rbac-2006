package cn.jasonone.bean;

import java.io.Serializable;
import java.time.Period;
import java.util.Date;
import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
    * 权限信息表
    */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Permission implements Serializable {
    /**
    * 主键
    */
    private Integer id;

    /**
    * 父权限ID
    */
    private Integer pid;

    /**
    * 角色名称
    */
    private String name;

    /**
    * 角色代码
    */
    private String code;

    /**
    * 权限参数
    */
    private String param;

    /**
    * 权限类型
    */
    private Integer type;

    /**
    * 状态
    */
    private Integer status;

    /**
    * 创建时间
    */
    private Date createTime;

    /**
    * 最后修改时间
    */
    private Date updateTime;
    /**
     * 子权限列表
     */
    private List<Permission> childs;
    /**
     * 父权限
     */
    private Permission parent;

    private static final long serialVersionUID = 1L;
}