package cn.jasonone.bean;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Role implements Serializable {
    /**
    * 主键
    */
    private Integer id;

    /**
    * 角色名称
    */
    private String name;

    /**
    * 角色代码
    */
    private String code;

    /**
    * 状态
    */
    private Integer status;

    /**
    * 创建时间
    */
    private Date createTime;

    /**
    * 最后修改时间
    */
    private Date updateTime;
    
    private List<Permission> permissions;

    private static final long serialVersionUID = 1L;
}