package cn.jasonone.bean;

import java.io.Serializable;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
    * 用户角色映射表
    */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class UserRoleMapping implements Serializable {
    /**
    * 用户主键
    */
    private Integer userId;

    /**
    * 角色主键
    */
    private Integer roleId;

    private static final long serialVersionUID = 1L;
}