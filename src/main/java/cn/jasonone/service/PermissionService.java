package cn.jasonone.service;

import cn.jasonone.bean.Permission;
import java.util.List;
import cn.jasonone.bean.PermissionExample;
public interface PermissionService{


    long countByExample(PermissionExample example);

    int deleteByExample(PermissionExample example);

    int deleteByPrimaryKey(Integer id);

    int insertSelective(Permission record);

    List<Permission> selectByExample(PermissionExample example);

    Permission selectByPrimaryKey(Integer id);

    int updateByExampleSelective(Permission record,PermissionExample example);

    int updateByExample(Permission record,PermissionExample example);

    int updateByPrimaryKeySelective(Permission record);

}
