package cn.jasonone.service;

import java.util.List;
import cn.jasonone.bean.RolePermissionMappingExample;
import cn.jasonone.bean.RolePermissionMapping;
public interface RolePermissionMappingService{


    long countByExample(RolePermissionMappingExample example);

    int deleteByExample(RolePermissionMappingExample example);

    int insertSelective(RolePermissionMapping record);

    List<RolePermissionMapping> selectByExample(RolePermissionMappingExample example);

    int updateByExampleSelective(RolePermissionMapping record,RolePermissionMappingExample example);

    int updateByExample(RolePermissionMapping record,RolePermissionMappingExample example);

}
