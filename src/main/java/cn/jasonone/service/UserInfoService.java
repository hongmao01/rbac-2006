package cn.jasonone.service;

import java.util.List;
import cn.jasonone.bean.UserInfoExample;
import cn.jasonone.bean.UserInfo;
import cn.jasonone.vo.UserInfoVO;

public interface UserInfoService{


    long countByExample(UserInfoExample example);

    int deleteByExample(UserInfoExample example);

    int deleteByPrimaryKey(Integer id);

    void insertSelective(UserInfoVO record);

    List<UserInfo> selectByExample(UserInfoExample example);

    UserInfo selectByPrimaryKey(Integer id);

    int updateByExampleSelective(UserInfo record,UserInfoExample example);

    int updateByExample(UserInfo record,UserInfoExample example);

    void updateByPrimaryKeySelective(UserInfoVO record);
}
