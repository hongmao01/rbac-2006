package cn.jasonone.service;

import cn.jasonone.bean.Role;
import java.util.List;
import cn.jasonone.bean.RoleExample;
import cn.jasonone.vo.RoleVO;

public interface RoleService{


    long countByExample(RoleExample example);

    int deleteByExample(RoleExample example);

    int deleteByPrimaryKey(Integer id);

    void insertSelective(RoleVO record);

    List<Role> selectByExample(RoleExample example);

    Role selectByPrimaryKey(Integer id);

    int updateByExampleSelective(Role record,RoleExample example);

    int updateByExample(Role record,RoleExample example);

    void updateByPrimaryKeySelective(RoleVO record);

}
