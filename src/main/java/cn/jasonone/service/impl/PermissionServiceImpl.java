package cn.jasonone.service.impl;

import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import cn.jasonone.bean.Permission;
import java.util.List;
import cn.jasonone.mapper.PermissionMapper;
import cn.jasonone.bean.PermissionExample;
import cn.jasonone.service.PermissionService;
@Service("permissionService")
public class PermissionServiceImpl implements PermissionService{

    @Resource
    private PermissionMapper permissionMapper;

    @Override
    public long countByExample(PermissionExample example) {
        return permissionMapper.countByExample(example);
    }

    @Override
    public int deleteByExample(PermissionExample example) {
        return permissionMapper.deleteByExample(example);
    }

    @Override
    public int deleteByPrimaryKey(Integer id) {
        return permissionMapper.deleteByPrimaryKey(id);
    }

    @Override
    public int insertSelective(Permission record) {
        return permissionMapper.insertSelective(record);
    }

    @Override
    public List<Permission> selectByExample(PermissionExample example) {
        return permissionMapper.selectByExample(example);
    }

    @Override
    public Permission selectByPrimaryKey(Integer id) {
        return permissionMapper.selectByPrimaryKey(id);
    }

    @Override
    public int updateByExampleSelective(Permission record,PermissionExample example) {
        return permissionMapper.updateByExampleSelective(record,example);
    }

    @Override
    public int updateByExample(Permission record,PermissionExample example) {
        return permissionMapper.updateByExample(record,example);
    }

    @Override
    public int updateByPrimaryKeySelective(Permission record) {
        return permissionMapper.updateByPrimaryKeySelective(record);
    }

}
