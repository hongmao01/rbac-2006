package cn.jasonone.service.impl;

import cn.jasonone.bean.RolePermissionMapping;
import cn.jasonone.bean.RolePermissionMappingExample;
import cn.jasonone.service.RolePermissionMappingService;
import cn.jasonone.vo.RoleVO;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import cn.jasonone.bean.Role;
import java.util.List;
import cn.jasonone.mapper.RoleMapper;
import cn.jasonone.bean.RoleExample;
import cn.jasonone.service.RoleService;
import org.springframework.transaction.annotation.Transactional;

@Service("roleService")
public class RoleServiceImpl implements RoleService{

    @Resource
    private RoleMapper roleMapper;
    @Resource
    private RolePermissionMappingService rolePermissionMappingService;

    @Override
    public long countByExample(RoleExample example) {
        return roleMapper.countByExample(example);
    }

    @Override
    public int deleteByExample(RoleExample example) {
        return roleMapper.deleteByExample(example);
    }

    @Override
    public int deleteByPrimaryKey(Integer id) {
        return roleMapper.deleteByPrimaryKey(id);
    }
    
    @Override
    @Transactional
    public void insertSelective(RoleVO record) {
        if(roleMapper.insertSelective(record)<1){
            throw new RuntimeException("新增角色失败");
        }
    
        int[] permissionIds = record.getPermissionIds();
    
        if (permissionIds != null) {
            for (int permissionId : permissionIds) {
                RolePermissionMapping rpm = new RolePermissionMapping(record.getId(), permissionId);
                this.rolePermissionMappingService.insertSelective(rpm);
            }
        }
    
    }

    @Override
    public List<Role> selectByExample(RoleExample example) {
        return roleMapper.selectByExample(example);
    }

    @Override
    public Role selectByPrimaryKey(Integer id) {
        return roleMapper.selectByPrimaryKey(id);
    }

    @Override
    public int updateByExampleSelective(Role record,RoleExample example) {
        return roleMapper.updateByExampleSelective(record,example);
    }

    @Override
    public int updateByExample(Role record,RoleExample example) {
        return roleMapper.updateByExample(record,example);
    }

    @Override
    public void updateByPrimaryKeySelective(RoleVO record) {
        int[] permissionIds = record.getPermissionIds();
        //清除原有权限关联关系
        RolePermissionMappingExample example = new RolePermissionMappingExample();
        example.createCriteria().andRoleIdEqualTo(record.getId());
        this.rolePermissionMappingService.deleteByExample(example);
        // 新增新的权限关联关系
        if (permissionIds != null) {
            for (int permissionId : permissionIds) {
                RolePermissionMapping rpm = new RolePermissionMapping(record.getId(), permissionId);
                this.rolePermissionMappingService.insertSelective(rpm);
            }
        }
        roleMapper.updateByPrimaryKeySelective(record);
    }

}
