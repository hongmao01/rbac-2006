package cn.jasonone.service.impl;

import cn.jasonone.bean.UserRoleMapping;
import cn.jasonone.bean.UserRoleMappingExample;
import cn.jasonone.mapper.RoleMapper;
import cn.jasonone.service.RoleService;
import cn.jasonone.service.UserRoleMappingService;
import cn.jasonone.vo.UserInfoVO;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import java.util.List;
import cn.jasonone.mapper.UserInfoMapper;
import cn.jasonone.bean.UserInfoExample;
import cn.jasonone.bean.UserInfo;
import cn.jasonone.service.UserInfoService;
import org.springframework.transaction.annotation.Transactional;

@Service("userInfoService")
public class UserInfoServiceImpl implements UserInfoService{

    @Resource
    private UserInfoMapper userInfoMapper;
    @Resource
    private UserRoleMappingService userRoleMappingService;

    @Override
    public long countByExample(UserInfoExample example) {
        return userInfoMapper.countByExample(example);
    }

    @Override
    public int deleteByExample(UserInfoExample example) {
        return userInfoMapper.deleteByExample(example);
    }

    @Override
    public int deleteByPrimaryKey(Integer id) {
        return userInfoMapper.deleteByPrimaryKey(id);
    }

    @Override
    public void insertSelective(UserInfoVO record) {
        //    新增新的角色关联信息
        int[] roleIds = record.getRoleIds();
        if(roleIds == null || roleIds.length<1){
            throw new RuntimeException("必须关联至少一个角色");
        }
        userInfoMapper.insertSelective(record);
        for (int roleId : roleIds) {
            UserRoleMapping urm = new UserRoleMapping(record.getId(), roleId);
            this.userRoleMappingService.insertSelective(urm);
        }
        
    }

    @Override
    public List<UserInfo> selectByExample(UserInfoExample example) {
        return userInfoMapper.selectByExample(example);
    }

    @Override
    public UserInfo selectByPrimaryKey(Integer id) {
        return userInfoMapper.selectByPrimaryKey(id);
    }

    @Override
    public int updateByExampleSelective(UserInfo record,UserInfoExample example) {
        return userInfoMapper.updateByExampleSelective(record,example);
    }

    @Override
    public int updateByExample(UserInfo record,UserInfoExample example) {
        return userInfoMapper.updateByExample(record,example);
    }

    @Override
    @Transactional
    public void updateByPrimaryKeySelective(UserInfoVO record) {
        // 更新角色关联信息
        UserRoleMappingExample example = new UserRoleMappingExample();
        //    删除旧的角色关联信息
        example.createCriteria().andUserIdEqualTo(record.getId());
        if(userRoleMappingService.deleteByExample(example)<1){
            throw new RuntimeException("修改用户角色信息失败");
        }
        //    新增新的角色关联信息
        int[] roleIds = record.getRoleIds();
        if(roleIds == null || roleIds.length<1){
            throw new RuntimeException("必须关联至少一个角色");
        }
        for (int roleId : roleIds) {
            UserRoleMapping urm = new UserRoleMapping(record.getId(), roleId);
            this.userRoleMappingService.insertSelective(urm);
        }
        // 更新用户信息
        userInfoMapper.updateByPrimaryKeySelective(record);
    }

}
