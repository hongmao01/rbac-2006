package cn.jasonone.service.impl;

import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import java.util.List;
import cn.jasonone.bean.RolePermissionMappingExample;
import cn.jasonone.mapper.RolePermissionMappingMapper;
import cn.jasonone.bean.RolePermissionMapping;
import cn.jasonone.service.RolePermissionMappingService;
@Service
public class RolePermissionMappingServiceImpl implements RolePermissionMappingService{

    @Resource
    private RolePermissionMappingMapper rolePermissionMappingMapper;

    @Override
    public long countByExample(RolePermissionMappingExample example) {
        return rolePermissionMappingMapper.countByExample(example);
    }

    @Override
    public int deleteByExample(RolePermissionMappingExample example) {
        return rolePermissionMappingMapper.deleteByExample(example);
    }

    @Override
    public int insertSelective(RolePermissionMapping record) {
        return rolePermissionMappingMapper.insertSelective(record);
    }

    @Override
    public List<RolePermissionMapping> selectByExample(RolePermissionMappingExample example) {
        return rolePermissionMappingMapper.selectByExample(example);
    }

    @Override
    public int updateByExampleSelective(RolePermissionMapping record,RolePermissionMappingExample example) {
        return rolePermissionMappingMapper.updateByExampleSelective(record,example);
    }

    @Override
    public int updateByExample(RolePermissionMapping record,RolePermissionMappingExample example) {
        return rolePermissionMappingMapper.updateByExample(record,example);
    }

}
