package cn.jasonone.service.impl;

import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import java.util.List;
import cn.jasonone.bean.UserRoleMappingExample;
import cn.jasonone.mapper.UserRoleMappingMapper;
import cn.jasonone.bean.UserRoleMapping;
import cn.jasonone.service.UserRoleMappingService;
@Service
public class UserRoleMappingServiceImpl implements UserRoleMappingService{

    @Resource
    private UserRoleMappingMapper userRoleMappingMapper;

    @Override
    public long countByExample(UserRoleMappingExample example) {
        return userRoleMappingMapper.countByExample(example);
    }

    @Override
    public int deleteByExample(UserRoleMappingExample example) {
        return userRoleMappingMapper.deleteByExample(example);
    }

    @Override
    public int insertSelective(UserRoleMapping record) {
        return userRoleMappingMapper.insertSelective(record);
    }

    @Override
    public List<UserRoleMapping> selectByExample(UserRoleMappingExample example) {
        return userRoleMappingMapper.selectByExample(example);
    }

    @Override
    public int updateByExampleSelective(UserRoleMapping record,UserRoleMappingExample example) {
        return userRoleMappingMapper.updateByExampleSelective(record,example);
    }

    @Override
    public int updateByExample(UserRoleMapping record,UserRoleMappingExample example) {
        return userRoleMappingMapper.updateByExample(record,example);
    }

}
