package cn.jasonone.service;

import java.util.List;
import cn.jasonone.bean.UserRoleMappingExample;
import cn.jasonone.bean.UserRoleMapping;
public interface UserRoleMappingService{


    long countByExample(UserRoleMappingExample example);

    int deleteByExample(UserRoleMappingExample example);

    int insertSelective(UserRoleMapping record);

    List<UserRoleMapping> selectByExample(UserRoleMappingExample example);

    int updateByExampleSelective(UserRoleMapping record,UserRoleMappingExample example);

    int updateByExample(UserRoleMapping record,UserRoleMappingExample example);

}
