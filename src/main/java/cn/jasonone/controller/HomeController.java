package cn.jasonone.controller;

import cn.jasonone.bean.Permission;
import cn.jasonone.bean.Role;
import cn.jasonone.bean.UserInfo;
import cn.jasonone.bean.UserInfoExample;
import cn.jasonone.service.UserInfoService;
import cn.jasonone.vo.HttpResult;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.DigestUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.bind.support.SessionStatus;

import javax.annotation.Resource;
import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller
@SessionAttributes({"LOGIN_STATUS","PERMISSIONS","AUTHORIZATIONS"})
public class HomeController {
	@Resource
	private UserInfoService userInfoService;
	@GetMapping("/")
	public String index(){
		return "index";
	}
	
	@RequestMapping("/logout")
	public String logout(SessionStatus status){
		status.setComplete();//注销Session
		return "login";
	}
	
	@GetMapping("/login")
	public String login(){
		return "login";
	}
	@PostMapping("/login")
	@ResponseBody
	public HttpResult login(UserInfo userInfo, Model model){
		if(userInfo == null 
				|| userInfo.getUsername() == null || userInfo.getUsername().isEmpty()
				|| userInfo.getPassword() == null || userInfo.getPassword().isEmpty()
		){
			return new HttpResult(10001, "用户名或密码不能为空");
		}
		UserInfoExample example = new UserInfoExample();
		UserInfoExample.Criteria criteria = example.createCriteria();
		criteria.andUsernameEqualTo(userInfo.getUsername());
		criteria.andPasswordEqualTo(DigestUtils.md5DigestAsHex(userInfo.getPassword().getBytes()));
		List<UserInfo> userInfos = this.userInfoService.selectByExample(example);
		if(userInfos.size() != 1){
			return new HttpResult(10002, "用户名或密码错误");
		}else{
			userInfo = userInfos.get(0);
			model.addAttribute("LOGIN_STATUS", userInfo);
			List<Role> roles = userInfo.getRoles();
			List<Permission> permissions = new ArrayList<>();
			Map<Integer,Permission> permissionMap=new HashMap<>();
			// 整理用户拥有的所有权限
			for (Role role : roles) {
				List<Permission> permissionList = role.getPermissions();
				if (permissionList != null) {
					for (Permission permission : permissionList) {
						permission.setChilds(null);
						permissionMap.put(permission.getId(),permission);
					}
				}
			}
			
			for (Permission permission : permissionMap.values()) {
				if(permissionMap.containsKey(permission.getPid())){
					Permission parent = permissionMap.get(permission.getPid());
					List<Permission> childs = parent.getChilds();
					if(childs == null){
						childs = new ArrayList<>();
						parent.setChilds(childs);
					}
					childs.add(permission);
				}else{
					permissions.add(permission);
				}
			}
			
			model.addAttribute("PERMISSIONS", permissions);
			model.addAttribute("AUTHORIZATIONS", new ArrayList<>(permissionMap.values()));
			return HttpResult.SUCCESS;
		}
	}
	
	
}
