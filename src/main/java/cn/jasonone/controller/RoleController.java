package cn.jasonone.controller;

import cn.jasonone.bean.Role;
import cn.jasonone.bean.RoleExample;
import cn.jasonone.service.RoleService;
import cn.jasonone.service.RoleService;
import cn.jasonone.vo.HttpResult;
import cn.jasonone.vo.RoleVO;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.util.DigestUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.annotation.Resource;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

//@Controller
//@ResponseBody
@RestController
@RequestMapping("/role")
public class RoleController {
	@Resource(name = "roleService")
	private RoleService baseService;
	
	private String viewRoot() {
		return "roleManager/";
	}
	
	@GetMapping(produces = "text/html")
	public ModelAndView index() {
		return new ModelAndView(viewRoot() + "index");
	}
	
	@GetMapping(path = "/insert", produces = "text/html")
	public ModelAndView insert() {
		return new ModelAndView(viewRoot() + "insert");
	}
	
	@GetMapping(produces = "application/json")
	public HttpResult<Role> findAll(
			@RequestParam(defaultValue = "1") int page,
			@RequestParam(defaultValue = "10") int limit,
			Role role
	) {
		PageHelper.startPage(page, limit);
		
		RoleExample example = new RoleExample();
		// 创建条件
		RoleExample.Criteria criteria = example.createCriteria();
		
		// where (username = ? and password =? ) or ( ... )
		if (role != null) {
			if (role.getName() != null && !role.getName().isEmpty()) {
				criteria.andNameLike("%" + role.getName() + "%");
			}
			if (role.getCode() != null && !role.getCode().isEmpty()) {
				criteria.andCodeLike("%" + role.getCode() + "%");
			}
			if (role.getStatus() != null) {
				criteria.andStatusEqualTo(role.getStatus());
			}
		}
		
		List<Role> roles = baseService.selectByExample(example);
		PageInfo<Role> pageInfo = new PageInfo<>(roles);
		HttpResult rs = new HttpResult();
		rs.setData(roles);
		rs.setCount(pageInfo.getTotal());
		return rs;
	}
	
	@PutMapping("/insert")
	public HttpResult insert(@RequestBody RoleVO role) {
		try {
			this.baseService.insertSelective(role);
			return HttpResult.SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
			return new HttpResult(30001, "角色新增失败:" + e.getMessage());
		}
	}
	
	/**
	 * 批量删除
	 *
	 * @param ids
	 * @return
	 */
	@DeleteMapping("/delete")
	public HttpResult delete(Integer[] ids) {
		RoleExample example = new RoleExample();
		example.createCriteria().andIdIn(Arrays.asList(ids));
		try {
			if (this.baseService.deleteByExample(example) > 0) {
				return HttpResult.SUCCESS;
			}
		} catch (Exception e) {
			e.printStackTrace();
			return new HttpResult(30002, "删除失败:" + e.getMessage());
		}
		return new HttpResult(30002, "删除失败");
	}
	
	@DeleteMapping("/delete/{id}")
	public HttpResult delete(@PathVariable int id) {
		try {
			int row = this.baseService.deleteByPrimaryKey(id);
			if (row > 0) {
				return HttpResult.SUCCESS;
			}
		} catch (Exception e) {
			e.printStackTrace();
			return new HttpResult(30002, "删除失败:" + e.getMessage());
		}
		return new HttpResult(30002, "删除失败");
	}
	
	@GetMapping("/update/{id}")
	public ModelAndView update(@PathVariable int id) {
		ModelAndView mv = new ModelAndView(viewRoot() + "update");
		Role role = this.baseService.selectByPrimaryKey(id);
		mv.addObject("entity", role);
		mv.addObject("permissionIds", role.getPermissions().stream().map(permission -> permission.getId().toString()).collect(Collectors.joining(",")));
		return mv;
	}
	
	@PostMapping("/update")
	public HttpResult update(@RequestBody RoleVO role) {
		try {
			this.baseService.updateByPrimaryKeySelective(role);
			return HttpResult.SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
			return new HttpResult(30003, "修改失败:" + e.getMessage());
		}
	}
}
