package cn.jasonone.interceptors;

import cn.jasonone.annotations.Authorization;
import cn.jasonone.bean.Permission;
import cn.jasonone.bean.Role;
import cn.jasonone.bean.UserInfo;
import org.springframework.stereotype.Component;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

@Component
public class AuthorizationInterceptor implements HandlerInterceptor {
	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
		
		if (handler instanceof HandlerMethod) {
			HandlerMethod method = (HandlerMethod) handler;
			if (method.hasMethodAnnotation(Authorization.class)) {
				UserInfo loginStatus = (UserInfo) request.getSession().getAttribute("LOGIN_STATUS");
				List<Role> roles = loginStatus.getRoles();
				List<Permission> permissions = (List) request.getSession().getAttribute("AUTHORIZATIONS");
				Authorization authorization = method.getMethodAnnotation(Authorization.class);
				String[] role = authorization.role();
				for (String r : role) {
					for (Role role1 : roles) {
						if (role1.getCode().equals(r)) {
							return true;
						}
					}
				}
				String[] values = authorization.value();
				for (String value : values) {
					for (Permission permission : permissions) {
						if(permission.getCode().equals(value)){
							return true;
						}
					}
				}
				response.sendError(403, "您没有访问权限,请联系管理员");
				return false;
			}
		}
		return true;
	}
	
	@Override
	public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView) throws Exception {
	
	}
	
	@Override
	public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) throws Exception {
	
	}
}
